package undo

/**
 * A manager for undo and redo operations to [Document]s, based
 * on [Change] objects.
 */
interface UndoManager {
    /**
     * Registers a new change in this undo manager. If the buffer
     * size of the undo manager is filled, replace the oldest change
     * with the one provided to this method.
     *
     * @param change The change to register.
     */
    fun registerChange(change: Change?)

    /**
     * Returns `true` if there is currently a change that
     * can be undone, and `false` otherwise.
     */
    fun canUndo(): Boolean

    /**
     * Performs the undo operation of the current change.
     *
     * @throws IllegalStateException If the manager is in a state that
     * does not allow an undo (that is if either [.canUndo]
     * would have returned `false`, or the application
     * of the change failed).
     */
    fun undo()

    /**
     * Returns `true` if there is currently a change that
     * can be redone, and `false` otherwise.
     */
    fun canRedo(): Boolean

    /**
     * Performs the redo operation of the current change.
     *
     * @throws IllegalStateException If the manager is in a state that
     * does not allow an redo (that is if either [.canRedo]
     * would have returned `false`, or the application
     * of the change failed).
     */
    fun redo()
}