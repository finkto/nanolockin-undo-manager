package undo

/**
 * An abstraction of a document to be used with the [UndoManager].
 *
 */
interface Document {
    /**
     * Deletes a string from the document.
     *
     * @param pos The position to start deletion.
     * @param s The string to delete.
     * @throws IllegalStateException If the document doesn't have `s`
     * as `pos`.
     */
    fun delete(pos: Int, s: String?)

    /**
     * Inserts a string into the document.
     *
     * @param pos The position to insert the string at.
     * @param s The string to insert.
     * @throws IllegalStateException If `pos` is an illegal position
     * (that is, if document is shorter than that).
     */
    fun insert(pos: Int, s: String?)

    /**
     * Sets the dot (cursor) position of the document.
     *
     * @param pos The dot position to set.
     * @throws IllegalStateException If `pos` is an illegal position
     * (that is, if document is shorter than that).
     */
    fun setDot(pos: Int)
}