package undo

/**
 * A factory for [UndoManager]s.
 *
 */
interface UndoManagerFactory {
    /**
     * Creates an undo manager for a [Document].
     *
     * @param doc The document to create the [UndoManager] for.
     * @param bufferSize The number of [Change]es stored.
     * @return The [UndoManager] created.
     */
    fun createUndoManager(doc: Document?, bufferSize: Int): UndoManager?
}