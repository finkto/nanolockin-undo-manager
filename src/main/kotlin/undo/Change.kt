package undo

/**
 * An abstraction of a change that can be registered with an
 * [UndoManager] and applied to a [Document].
 */
interface Change {
    /**
     * Returns the type of the change.
     */
    val type: String?

    /**
     * Apply this change to the given document.
     *
     * @param doc The document to apply the change to.
     * @throws IllegalStateException If the change cannot be applied to `doc`
     * (that is if the document refuses the application of the change).
     */
    fun apply(doc: Document?)

    /**
     * Reverts this change in the given document.
     *
     * @param doc The document to revert the change in.
     * @throws IllegalStateException If the change cannot be reverted in `doc`
     * (that is if the document refuses the reversion of the change).
     */
    fun revert(doc: Document?)
}